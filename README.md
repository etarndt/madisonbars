**This mobile uses crowd-sourced information to give line length information at Madison,WI bars.**

The application uses a Google Firebase database to collect and aggregate user-submitted information.

**Run Instructions**

To run, type the following command in terminal while in project directory: ionic-v1 serve
